package com.rebel.cronjobs.dao;

import com.rebel.cronjobs.models.CronModel;
import org.springframework.data.repository.CrudRepository;

public interface CronRepository extends CrudRepository <CronModel,Long> {
    public CronModel findByCronName(String cronName);
}
