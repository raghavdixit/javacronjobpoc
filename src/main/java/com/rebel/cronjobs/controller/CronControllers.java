package com.rebel.cronjobs.controller;

import com.rebel.cronjobs.exception.FinalException;
import com.rebel.cronjobs.request.CronRequest;
import com.rebel.cronjobs.service.CronService;
import com.rebel.cronjobs.util.ResponseGeneratorUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CronControllers {
    @Autowired
    CronService objCronService;

    @PostMapping(path = "/insert-Cron", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> insertCron(@RequestBody CronRequest objCronRequest) throws FinalException {
        return new ResponseEntity<>(ResponseGeneratorUtil.okResponse(objCronService.insertCron(objCronRequest)), HttpStatus.OK);
    }

    @PostMapping(path = "/activate-cron/{cronName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> activateCron(@PathVariable("cronName") String cronName) throws FinalException {
        return new ResponseEntity<>(ResponseGeneratorUtil.okResponse(objCronService.activateCron(cronName, true)), HttpStatus.OK);
    }

    @PostMapping(path = "/deactivate-cron/{cronName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deactivateCron(@PathVariable("cronName") String cronName) throws FinalException {
        return new ResponseEntity<>(ResponseGeneratorUtil.okResponse(objCronService.activateCron(cronName, false)), HttpStatus.OK);
    }

    @PostMapping(path = "/getCronDetailsByCronName/{cronName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getCronDetailsByCronName(@PathVariable("cronName") String cronName) throws FinalException {
        return new ResponseEntity<>(ResponseGeneratorUtil.okResponse(objCronService.getCronDetails(cronName)), HttpStatus.OK);
    }

}
