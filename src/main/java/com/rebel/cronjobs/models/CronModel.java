package com.rebel.cronjobs.models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Entity(name = "CronJobs")
public class CronModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long cronID;
    private String cronName;
    private String description;
    private String cronExpression;
    private String requestUrl;
    private String requestType;
    private String callbackUrl;
    private Integer retryCount;
    private Integer delayBetweenRetries;
    private boolean isActive;
    private boolean modifyIfExists;
    @Column(columnDefinition = "JSON")
    private String param;
    @Column(columnDefinition = "JSON")
    private String headers;
    @Column(columnDefinition = "JSON")
    private String body;

    public long getCronID() {
        return cronID;
    }

    public void setCronID(long cronID) {
        this.cronID = cronID;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public Integer getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    public Integer getDelayBetweenRetries() {
        return delayBetweenRetries;
    }

    public void setDelayBetweenRetries(Integer delayBetweenRetries) {
        this.delayBetweenRetries = delayBetweenRetries;
    }

    public boolean IsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }


    public JsonNode getParam() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode actualObj = mapper.readTree(param);
        return actualObj;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public JsonNode getHeaders() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode actualObj = mapper.readTree(headers);
        return actualObj;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    public JsonNode getBody() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode actualObj = mapper.readTree(body);
        return actualObj;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCronName() {
        return cronName;
    }

    public void setCronName(String cronName) {
        this.cronName = cronName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }


    public boolean isModifyIfExists() {
        return modifyIfExists;
    }

    public void setModifyIfExists(boolean modifyIfExists) {
        this.modifyIfExists = modifyIfExists;
    }

    @Override
    public String toString() {
        return "CronModel{" +
                "cronName='" + cronName + '\'' +
                ", description='" + description + '\'' +
                ", cronExpression='" + cronExpression + '\'' +
                ", requestUrl='" + requestUrl + '\'' +
                ", requestType='" + requestType + '\'' +
                ", callbackurl='" + callbackUrl + '\'' +
                ", modifyIfExists=" + modifyIfExists +
                '}';
    }
}
