package com.rebel.cronjobs.response;

public class InsertCronResponse {
    private long CronID;
    private String cronName;
    private boolean isActive;

    public long getCronID() {
        return CronID;
    }

    public void setCronID(long cronID) {
        CronID = cronID;
    }

    public String getCronName() {
        return cronName;
    }

    public void setCronName(String cronName) {
        this.cronName = cronName;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
