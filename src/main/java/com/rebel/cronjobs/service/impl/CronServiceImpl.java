package com.rebel.cronjobs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.rebel.cronjobs.dao.CronRepository;
import com.rebel.cronjobs.exception.FinalException;
import com.rebel.cronjobs.models.CronModel;
import com.rebel.cronjobs.request.CronRequest;
import com.rebel.cronjobs.response.InsertCronResponse;
import com.rebel.cronjobs.service.CronService;

@Service
public class CronServiceImpl implements CronService {
    @Autowired
    CronRepository objCronRepository;

    public InsertCronResponse insertCron(CronRequest objCronRequest) throws FinalException {
        CronModel objCronModel = new CronModel();
        objCronModel.setCronName(objCronRequest.getCronName());
        objCronModel.setCronExpression(objCronRequest.getCronExpression());
        objCronModel.setDescription(objCronRequest.getDescription());
        objCronModel.setRequestUrl(objCronRequest.getRequestUrl());
        objCronModel.setRequestType(objCronRequest.getRequestType());
        objCronModel.setCallbackUrl(objCronRequest.getCallbackUrl());
        objCronModel.setRetryCount(objCronRequest.getRetryCount());
        objCronModel.setDelayBetweenRetries(objCronRequest.getDelayBetweenRetries());
        objCronModel.setIsActive(true);
        objCronModel.setModifyIfExists(true);
        objCronModel.setParam(objCronRequest.getParam().toString());
        objCronModel.setHeaders(objCronRequest.getHeaders().toString());
        objCronModel.setBody(objCronRequest.getBody().toString());
        InsertCronResponse objInsertCronResponse = new InsertCronResponse();
        CronModel objCronModelrecord = null;
        try {
            objCronModelrecord = objCronRepository.save(objCronModel);
            if (objCronModelrecord == null)
                throw new FinalException(404, "notfound");
            objInsertCronResponse.setCronID(objCronModel.getCronID());
            objInsertCronResponse.setCronName(objCronModel.getCronName());
            objInsertCronResponse.setActive(objCronModel.IsActive());
            return objInsertCronResponse;
        } catch (DataIntegrityViolationException ex) {
            throw new FinalException(400, ex.getRootCause().getMessage());
        }
    }

    @Override
    public CronModel activateCron(String cronName, boolean flag) throws FinalException {
        CronModel cronModel = objCronRepository.findByCronName(cronName);
        cronModel.setIsActive(flag);
        cronModel = objCronRepository.save(cronModel);
        return cronModel;
    }

    @Override
    public CronModel getCronDetails(String cronName) {
        return objCronRepository.findByCronName(cronName);
    }
}
