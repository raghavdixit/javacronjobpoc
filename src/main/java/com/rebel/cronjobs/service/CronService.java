package com.rebel.cronjobs.service;

import com.rebel.cronjobs.exception.FinalException;
import com.rebel.cronjobs.models.CronModel;
import com.rebel.cronjobs.request.CronRequest;
import com.rebel.cronjobs.response.InsertCronResponse;

public interface CronService {
    InsertCronResponse insertCron(CronRequest objCronRequest) throws FinalException;

    CronModel activateCron(String objCronRequest, boolean flag) throws FinalException;

    CronModel getCronDetails(String cronName);
}
