package com.rebel.cronjobs.util;

import com.rebel.cronjobs.config.CronConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

@Component
public class Utilities {
	@Autowired
	CronConfig objCronConfig;
	private Properties urlProperties;

	public void test() {
		System.out.println("Utilities Class");
		System.out.println(objCronConfig.currentEnvironment);
	}

	public String getCompleteUrl() {
		loadConfigProperties();
		return urlProperties.getProperty("thanos-" + objCronConfig.currentEnvironment);
	}

	private void loadConfigProperties() {
		try {
			if (urlProperties == null) {
				System.out.println("Inside Load Properties");
				urlProperties = new Properties();
				urlProperties.load(new FileInputStream("src/main/resources/url.properties"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
