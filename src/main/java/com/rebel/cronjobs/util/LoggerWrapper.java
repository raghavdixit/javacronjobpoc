package com.rebel.cronjobs.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggerWrapper {

	private Logger LOGGER;

	public LoggerWrapper(Class<?> clazz) {
		LOGGER = LogManager.getLogger(clazz);
	}

	public void debug(Object message) {
		LOGGER.debug(message);
	}

	public void error(Object message) {
		LOGGER.error(message);
	}

	public void info(Object message) {
		LOGGER.info(message);
	}
}
