package com.rebel.cronjobs.util;

import com.rebel.cronjobs.exception.FinalException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionAdvice extends ResponseEntityExceptionHandler {

	private static final Logger LOGGER = LogManager.getLogger(ExceptionAdvice.class);

	@ExceptionHandler(value = { RuntimeException.class })
	protected ResponseEntity<Object> genericRuntimeExceptionHandler(RuntimeException ex, WebRequest request) {
		LOGGER.error("Runtime Exception: " + ex);
		return new ResponseEntity<Object>(ResponseGeneratorUtil.genericErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = { FinalException.class })
	protected ResponseEntity<Object> finalExceptionHandler(FinalException ex, WebRequest request) {
		LOGGER.error("Final Exception: " + ex);
		return new ResponseEntity<Object>(ResponseGeneratorUtil.genericErrorResponse(ex), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(value = { Exception.class })
	protected ResponseEntity<Object> genericExceptionHandler(Exception ex, WebRequest request) {
		LOGGER.error("Generic Exception: " + ex);
		return new ResponseEntity<Object>(ResponseGeneratorUtil.genericErrorResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
