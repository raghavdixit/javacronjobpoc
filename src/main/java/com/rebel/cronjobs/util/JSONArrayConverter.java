package com.rebel.cronjobs.util;

import org.json.JSONArray;
import org.json.JSONException;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class JSONArrayConverter implements AttributeConverter<JSONArray, String> {

    @Override
    public String convertToDatabaseColumn(JSONArray attribute) {
        String json;
        try {
            json = attribute.toString();
        } catch (NullPointerException ex) {
            //extend error handling here if you want
            json = "";
        }
        return json;
    }

    @Override
    public JSONArray convertToEntityAttribute(String dbData) {
        JSONArray jsonData;
        try {
            jsonData = new JSONArray(dbData);
        } catch (JSONException ex) {
            //extend error handling here if you want
            jsonData = null;
        }
        return jsonData;
    }
}
