package com.rebel.cronjobs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@EntityScan("com.rebel.cronjobs.models")
@EnableJpaRepositories("com.rebel.cronjobs.dao")
@SpringBootApplication
public class CronjobsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CronjobsApplication.class, args);
    }

}
